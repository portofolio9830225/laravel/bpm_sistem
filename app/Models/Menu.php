<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;
    protected $table = 'acl_menu';
    protected $guarded = ['id'];

    public static function subMenu($parent)
    {
        return self::where('is_parent', 0)->where('parent_id', $parent)->get();
    }
}
