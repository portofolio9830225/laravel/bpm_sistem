<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DokumenUpload extends Model
{
    use HasFactory;
    protected $table = 'dokumen_upload';
    protected $guarded = ['id'];
}
