<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HasilAuditTemporary extends Model
{
    use HasFactory;
    protected $table = 'hasil_audit_temporary';
    protected $guarded = ['id'];

    public static function fakultasProdi($fakultas, $prodi)
    {
        return self::where('fakultas_id', $fakultas)->where('prodi_id', $prodi)->first();
    }
}
