<?php

namespace App\Http\Livewire\C1;

use App\Models\Dokumen;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class IndikatorUtama extends Component
{
    use WithFileUploads;

    public $create = false;
    public $dokumen_kode = 'Indikator Kinerja Utama';
    public $sub_modul_id = 1;
    public $dokumen_nama, $file, $dokumen_namaEdit, $idEdit;

    public function render()
    {
        $data = Dokumen::where('dokumen_kode', 'Indikator Kinerja Utama')->get();
        return view('livewire.c1.indikator-utama', [
            'data'  => $data
        ]);
    }

    public function store()
    {
        $validatedData = $this->validate([
            'dokumen_kode'  => 'nullable',
            'sub_modul_id'  => 'nullable',
            'dokumen_nama'  => 'nullable',
        ]);

        $nama = $this->file->store('file', 'public');

        DB::beginTransaction();
        $validatedData['dokumen_deskripsi'] = $nama;
        Dokumen::create($validatedData);

        DB::commit();

        $this->emit('indikatorUtama');
    }

    public function delete($id)
    {
        $dokumen = Dokumen::find($id);

        DB::beginTransaction();
        if (Storage::exists('public/' . $dokumen->dokumen_deskripsi)) {
            Storage::delete('public/' . $dokumen->dokumen_deskripsi);
        }
        $dokumen->delete();
        // Dokumen::destroy($id);
        DB::commit();
    }

    public function edit($id)
    {
        DB::beginTransaction();
        $dokumen = Dokumen::find($id);
        $this->idEdit = $dokumen->id;
        $this->dokumen_namaEdit = $dokumen->dokumen_nama;
        DB::commit();
    }

    public function update()
    {
        DB::beginTransaction();
        Dokumen::find($this->idEdit)->update([
            'dokumen_nama'  => $this->dokumen_namaEdit
        ]);
        DB::commit();

        $this->emit('indikatorUtama');
    }
}
