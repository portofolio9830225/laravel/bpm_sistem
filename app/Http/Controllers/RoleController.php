<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Models\MenuRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->permissionKey = 'role';
        $this->middleware(['permission:' . $this->permissionKey . '-view']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'title' => 'Role',
            'data'  => Role::get()
        ];

        return view('role.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role = Role::create(['name' => request()->name]);
        create_menu($role->id);

        return redirect('role')->with('success', 'Role Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findById(dec($id));
        MenuRole::where('role_id', $role->id)->delete();
        $role->delete();

        return redirect('role')->with('success', 'Role Berhasil Dihapus');
    }

    public function access_menu($id)
    {
        // return dec($id);
        $role = Role::findById(dec($id));

        $data = [
            'title'         => 'Access Menu Role',
            'role'          => $role,
            'access_menu'   => menuByRole($role->id)
        ];

        return view('role.accessMenu', $data);
    }

    public function access_menu_update($id)
    {
        // return request();
        $role = Role::findById(dec($id));
        $role->syncPermissions();

        try {
            DB::beginTransaction();

            foreach (request()->permission as $val) {
                $role->givePermissionTo($val);
            }

            DB::commit();
            return redirect('role')->with('success', 'Role Access Berhasil Update');
        } catch (\Exception $e) {
            DB::rollBack();
            throw new UnprocessableEntityHttpException($e->getMessage());
        }
    }
}
