<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Fakultas;
use App\Models\Prodi;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'title' => 'Users',
            'users' => User::latest()->get()
        ];

        return view('users.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'title'     => 'Users',
            'roles'     => Role::get()->pluck('name'),
            'fakultas'  => Fakultas::get()
        ];

        return view('users.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'password' => 'same:konfirmasi_password'
        ], [
            'password.same' => 'Password tidak sama'
        ]);

        return request();

        try {
            DB::beginTransaction();

            $data = request()->all();
            if (request()->has('foto')) {
                $foto = request()->file('foto');
                $nama = time() . rand(1, 100) . '.' . $foto->getClientOriginalExtension();
                $foto->move('image/user/', $nama);

                $data['profile_photo'] = $nama;
            }
            $role = Role::findByName(request()->role);
            $data['role_id']    = $role->id;
            $data['password']   = Hash::make(request()->password);

            $user = User::create($data);
            $user->assignRole(request()->role);

            DB::commit();
            return redirect('users')->with('success', 'User Berhasil ditambah');
        } catch (\Exception $e) {
            DB::rollBack();
            throw new UnprocessableEntityHttpException($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();

            $user = User::find(dec($id));
            if ($user->profile_photo) {
                File::delete('image/user/' . $user->profile_photo);
            }

            $role = Role::findById($user->role_id);
            $user->removeRole($role->name);
            $user->delete();

            DB::commit();
            return redirect('users')->with('success', 'User Berhasil dihapus');
        } catch (\Exception $e) {
            DB::rollBack();
            throw new UnprocessableEntityHttpException($e->getMessage());
        }
    }

    public function fakultas($id)
    {
        $prodi = Prodi::byFakultas($id);
        return response()->json($prodi);
    }
}
