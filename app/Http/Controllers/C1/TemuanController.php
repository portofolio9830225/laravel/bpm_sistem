<?php

namespace App\Http\Controllers\C1;

use App\Http\Controllers\Controller;
use App\Models\Dokumen;
use App\Models\DokumenUpload;
use App\Models\Fakultas;
use App\Models\HasilAudit;
use App\Models\HasilAuditTemporary;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TemuanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'title'         => 'Temuan',
            'fakultas'      => Fakultas::get()
        ];

        if (Auth::user()->role_id == 4) {
            $data['dokumen']    = Dokumen::with('dokumen_upload')->where('fakultas_id', Auth::user()->fakultas_id)->where('prodi_id', Auth::user()->prodi_id)->get();
            $data['cekHasilAudit'] = HasilAuditTemporary::where('fakultas_id', Auth::user()->fakultas_id)->where('prodi_id', Auth::user()->prodi_id)->first();
            // return $data['cekHasilAudit'];
        }

        return view('c1.temuan.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return response()->json(123);

        DB::beginTransaction();

        HasilAuditTemporary::where('fakultas_id', request()->fakultas_id)->where('prodi_id', request()->prodi_id)->update([
            'status'    => 'selesai temuan'
        ]);
        DB::commit();

        return back()->with('success', 'Temuan berhasil dikirim ke prodi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dokumen = DokumenUpload::find(dec($id));
        $data = [
            'title'         => 'Temuan',
            'data'          => $dokumen,
            'klasifikasi'   => ['Minor', 'Mayor', 'Observasi']
        ];

        return view('c1.temuan.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return dec($id);
        $dokumen = DokumenUpload::find(dec($id));

        DB::beginTransaction();
        if (request()->hasFile('upload')) {
            deleteFile($dokumen->file, 'file/');
            $nama = uploadFile(request()->upload, 'file/');
            $dokumen->file = $nama;
            $dokumen->save();
        }
        DB::commit();

        return back()->with('success', 'Upload Dokumen berhasil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function file($fakultas_id, $prodi_id)
    {
        $dokumen    = Dokumen::with('dokumen_upload')->where('fakultas_id', $fakultas_id)->where('prodi_id', $prodi_id)->get();
        $hasilAudit = HasilAuditTemporary::where('fakultas_id', $fakultas_id)->where('prodi_id', $prodi_id)->first();
        // return $hasilAudit;

        if ($hasilAudit) {
            $status = $hasilAudit->status;
        } else {
            $status = '';
        }

        return response()->json([
            'status'    => $status,
            'data'      => $dokumen
        ]);
    }

    public function dokumen_upload()
    {
        // return request();
        $input = Arr::only(request()->all(), ['uraian', 'informasi']);

        if (request()->klasifikasi_id == 'Minor') {
            $input['klasifikasi_id'] = 1;
        } elseif (request()->klasifikasi_id == 'Mayor') {
            $input['klasifikasi_id'] = 2;
        } elseif (request()->klasifikasi_id == 'Observasi') {
            $input['klasifikasi_id'] = 3;
        }

        DokumenUpload::find(request()->dokumen_upload_id)->update($input);

        return response()->json([
            'status' => 'SUCCESS'
        ]);
    }
}
