<?php

namespace App\Http\Controllers\C1;

use App\Http\Controllers\Controller;
use App\Models\Fakultas;
use App\Models\HasilAuditTemporary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TindakLanjutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'title'         => 'Tindak Lanjut',
            'fakultas'      => Fakultas::get()
        ];

        if (Auth::user()->role_id == 4) {
            $data['hasilAudit'] = HasilAuditTemporary::where('fakultas_id', Auth::user()->fakultas_id)->where('prodi_id', Auth::user()->prodi_id)->first();
        }

        return view('c1.tindakLanjut.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hasilAuditTemporary = HasilAuditTemporary::fakultasProdi(request()->fakultas_id, request()->prodi_id);
        return $hasilAuditTemporary;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
