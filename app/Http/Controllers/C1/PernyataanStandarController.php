<?php

namespace App\Http\Controllers\C1;

use App\Http\Controllers\Controller;
use App\Models\Dokumen;
use App\Models\DokumenUpload;
use App\Models\HasilAuditTemporary;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Role;

class PernyataanStandarController extends Controller
{
    public function __construct()
    {
        // $this->middleware('permission:pernyataan-standard-view')->only(['index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'title'                 => 'Pernyataan Standard',
            'indikatorUtama'        => Dokumen::with('dokumen_upload')->where('dokumen_kode', 'Indikator Kinerja Utama')->first(),
            'indikatorTambahan'     => Dokumen::with('dokumen_upload')->where('dokumen_kode', 'Indikator Kinerja Tambahan')->first(),
            'strategiPencapaian'    => Dokumen::with('dokumen_upload')->where('dokumen_kode', 'Strategi Pencapaian')->first(),
            'realisasiPencapaian'   => Dokumen::with('dokumen_upload')->where('dokumen_kode', 'Realisasi Pencapaian')->first(),
            'faktorPendukung'       => Dokumen::with('dokumen_upload')->where('dokumen_kode', 'Faktor Pendukung Keberhasilan')->first(),
            'hasilAudit'            => HasilAuditTemporary::where('fakultas_id', Auth::user()->fakultas_id)->where('prodi_id', Auth::user()->prodi_id)->first()
        ];

        // return $data;

        return view('c1.pernyataanStandar.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputDokumen = Arr::except(request()->all(), ['_token', 'file', 'dokumen_upload_nama']);
        $inputDokumen['fakultas_id']    = Auth::user()->fakultas_id;
        $inputDokumen['prodi_id']       = Auth::user()->prodi_id;
        $inputDokumen['users_id']       = Auth::id();

        $inputDokumenUpload = Arr::only(request()->all(), ['dokumen_upload_nama']);

        DB::beginTransaction();

        // Cek dokumen_kode
        $dokumen = Dokumen::where('dokumen_kode', request()->dokumen_kode)->first();
        if (is_null($dokumen)) {
            $dokumen = Dokumen::create($inputDokumen);
        }

        $nama = uploadFile(request()->file, 'file/');
        $inputDokumenUpload['file']         = $nama;
        $inputDokumenUpload['dokumen_id']   = $dokumen->id;
        DokumenUpload::create($inputDokumenUpload);

        // Dokumen::create($input);
        DB::commit();

        return back()->with('success', 'File berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();

        $dokUpload  = DokumenUpload::find(dec($id));
        $dokumen    = DokumenUpload::where('dokumen_id', $dokUpload->dokumen_id)->get();

        if (count($dokumen) == 1) {
            Dokumen::destroy($dokUpload->dokumen_id);
        }

        deleteFile($dokUpload->file, 'file/');
        $dokUpload->delete();

        DB::commit();

        return back()->with('success', 'File berhasil dihapus');
        // $dokumen = Dokumen::find($id);
        // return Storage::download('public/' . $dokumen->dokumen_deskripsi);
    }

    public function kirim_berkas()
    {
        // return request();
        $input = request()->all();
        $input['status'] = 'temuan';
        HasilAuditTemporary::create($input);

        return redirect('c1/temuan')->with('success', 'Berkas berhasil dikirim');
    }

    public function download_pdf($id)
    {
        $dokUpload = DokumenUpload::find($id);
        $path = public_path('file/' . $dokUpload->file);
        // return $path;
        return response()->download($path);
    }

    public function tindak_lanjut()
    {
        // return request();
        HasilAuditTemporary::where('fakultas_id', request()->fakultas_id)->where('prodi_id', request()->prodi_id)->update([
            'status'            => 'tindak lanjut',
            'tanggal_pemenuhan' => request()->tanggal
        ]);

        return redirect('c1/temuan')->with('success', 'Berkas berhasil dikirim');
    }

    public function catatan()
    {
        return request();

        // Dokumen::
    }
}
