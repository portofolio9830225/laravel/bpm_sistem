<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AuthController extends Controller
{
    public function index()
    {
        // create_menu(1);
        return view('login');
        // return 123;
    }

    public function auth()
    {
        // return request();
        $user = User::where('email', request()->email)->first();
        $role = Role::findById($user->role_id);

        $data = Arr::only(request()->all(), ['email', 'password']);
        if (Auth::attempt($data)) {
            request()->session()->regenerate();

            return redirect('home');
        }

        return back()->with('error', 'Email / Password Salah');
    }

    public function logout()
    {
        Auth::logout();

        request()->session()->invalidate();

        request()->session()->regenerateToken();

        return redirect('/');
    }
}
