<?php

use App\Models\Menu;
use App\Models\MenuRole;
use App\Models\Modul;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Gate;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

function show_modules()
{
    $get_modul = DB::table('modul')->get();
    $base_url = substr(url()->current(), strlen(url('/')));
    $parse_url = explode('/', $base_url);
    $shift_array = array_shift($parse_url);

    if (count($parse_url) > 0) {
        $primary_url = $parse_url[0];
    } else {
        $primary_url = '/dsahboard';
    }

    foreach ($get_modul as $gm) {
        if ($gm->modul_url != NULL) {
            if (url($primary_url) == url($gm->modul_url)) {
                // $show_modules = '<li>
                //                     <a href="'.url($gm->modul_admin_url).'" class="ttr-material-button">
                //                         <span class="ttr-label">'.$gm->modul_admin_name.'</span>
                //                     </a>
                //                 </li>';
                $show_modules = '<li class="nav-item active">
                                    <a href="' . url($gm->modul_url) . '">
                                        <p>' . $gm->modul_name . '</p>
                                    </a>
                                </li>';
            } else {
                $show_modules = '<li class="nav-item">
                                    <a href="' . url($gm->modul_url) . '">
                                        <p>' . $gm->modul_name . '</p>
                                    </a>
                                </li>';
            }
        } else {
            $get_detail = DB::table('sub_modul')
                ->where([
                    ['modul_id', $gm->modul_id],
                ])
                ->get();
            $show_sub = '';
            foreach ($get_detail as $gd) {
                $show_sub .= '<li>
                                <a href="' . url($gd->sub_modul_url) . '" class="ttr-material-button">
                                <span class="ttr-label">' . $gd->sub_modul_name . '</span></a>
                            </li>';
            }
            $show_modules = '<li class="nav-item">
                                <a data-toggle="collapse" href="' . $gm->modul_slug . '">
                                    <p>' . $gm->modul_name . '</p>
                                    <span class="caret"></span>
                                </a>
                                <div class="collapse" id="' . $gm->modul_slug . '">
                                    <ul class="nav nav-collapse">
                                        ' . $show_sub . '
                                    </ul>
                                </div>
                            </li>';
        }
        echo $show_modules;
    }
}

function enc($val)
{
    return Crypt::encrypt($val);
}

function dec($val)
{
    return Crypt::decrypt($val);
}

function create_menu($role_id, $is_active = 0)
{
    $permissions = Permission::get()->pluck('name');
    $role = Role::findById($role_id);

    foreach ($permissions as $val) {
        $permission = Permission::findByName($val);
        $role->givePermissionTo($permission);
    }
    return $role;
}

function menu()
{
    $menu = Menu::where('is_parent', 1)->get();
    $arr_menu = [];
    foreach ($menu as $item) {
        if ($item->permisson_key == '') {
            // Ada Sub Menu
            $subMenus = Menu::subMenu($item->id);
            $arr_subMenu = [];
            foreach ($subMenus as $val) {
                if (Gate::allows($val->permisson_key . '-view')) {
                    $arr_subMenu[] = $val;
                }
            }

            if (count($arr_subMenu) != 0) {
                $item['sub_menu'] = $arr_subMenu;
                $arr_menu[] = $item;
            }
        } else {
            if (Gate::allows($item->permisson_key . '-view')) {
                $arr_menu[] = $item;
            }
        }
    }

    return $arr_menu;
}

function menuByRole($role_id)
{
    $role = Role::findById($role_id);
    $menu = Menu::where('is_parent', 1)->select('id', 'name_menu', 'permisson_key')->get();
    $access = [];
    foreach ($menu as $item) {
        if ($item->permisson_key == '') {
            // Ada Sub Menu
            $subMenus = Menu::subMenu($item->id);
            foreach ($subMenus as $val) {

                if ($role->hasPermissionTo($val->permisson_key . '-view')) {
                    $is_active = 1;
                } else {
                    $is_active = 0;
                }

                $access[] = [
                    'id_menu_role'  => $val->id,
                    'name_menu'     => $item->name_menu . ' > ' . $val->name_menu,
                    'is_active'     => $is_active,
                    'permission'    => $val->permisson_key
                ];
            }
        } else {
            if ($role->hasPermissionTo($item->permisson_key . '-view')) {
                $is_active = 1;
            } else {
                $is_active = 0;
            }

            $access[] = [
                'id_menu_role'  => $item->id,
                'name_menu'     => $item->name_menu,
                'is_active'     => $is_active,
                'permission'    => $item->permisson_key
            ];
        }
    }

    return $access;
}

function uploadFile($file, $tmptFile)
{
    $nama = time() . rand(1, 100) . '.' . $file->getClientOriginalExtension();
    $file->move($tmptFile, $nama);
    return $nama;
}

function deleteFile($file, $tmptFile)
{
    File::delete($tmptFile . $file);
}
