<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\FakultasController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProdiController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AuthController::class, 'index'])->name('login');
Route::post('login', [AuthController::class, 'auth']);

Route::middleware(['auth'])->group(function () {
    Route::get('home', [HomeController::class, 'index'])->name('home');

    Route::resource('users', UserController::class);
    Route::get('user/fakultas/{id}', [UserController::class, 'fakultas']);

    Route::resource('fakultas', FakultasController::class);
    Route::resource('program-studi', ProdiController::class);
    Route::resource('role', RoleController::class);
    Route::get('role/access-menu/{id}', [RoleController::class, 'access_menu']);
    Route::put('role/access-menu/{id}', [RoleController::class, 'access_menu_update']);

    Route::get('c1/pernyataan-standard/kirim_berkas', [App\Http\Controllers\C1\PernyataanStandarController::class, 'kirim_berkas']);
    Route::get('c1/pernyataan-standard/tindak_lanjut', [App\Http\Controllers\C1\PernyataanStandarController::class, 'tindak_lanjut']);
    Route::get('c1/pernyataan-standard/download/{id}', [App\Http\Controllers\C1\PernyataanStandarController::class, 'download_pdf']);
    Route::post('c1/pernyataan-standard/catatan', [App\Http\Controllers\C1\PernyataanStandarController::class, 'catatan']);
    Route::resource('c1/pernyataan-standard', App\Http\Controllers\C1\PernyataanStandarController::class);

    Route::resource('c1/temuan', App\Http\Controllers\C1\TemuanController::class);
    Route::get('c1/temuan/file/{fakultas_id}/{prodi_id}', [App\Http\Controllers\C1\TemuanController::class, 'file']);
    Route::post('c1/temuan/dokumen-upload', [App\Http\Controllers\C1\TemuanController::class, 'dokumen_upload']);

    Route::resource('c1/tindak-lanjut', App\Http\Controllers\C1\TindakLanjutController::class);

    Route::resource('c2/pernyataan-standard', App\Http\Controllers\C2\PernyataanStandarController::class);

    Route::resource('c3/pernyataan-standard', App\Http\Controllers\C3\PernyataanStandarController::class);

    Route::resource('c4/pernyataan-standard', App\Http\Controllers\C4\PernyataanStandarController::class);

    Route::resource('c5/pernyataan-standard', App\Http\Controllers\C5\PernyataanStandarController::class);

    Route::resource('c6/pernyataan-standard', App\Http\Controllers\C6\PernyataanStandarController::class);

    Route::resource('c7/pernyataan-standard', App\Http\Controllers\C7\PernyataanStandarController::class);

    Route::resource('c8/pernyataan-standard', App\Http\Controllers\C8\PernyataanStandarController::class);

    Route::resource('c9/pernyataan-standard', App\Http\Controllers\C9\PernyataanStandarController::class);

    Route::post('logout', [AuthController::class, 'logout']);
});
