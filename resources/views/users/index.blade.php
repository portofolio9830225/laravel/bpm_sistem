@extends('layouts.layout')
@section('breadcrumb')
    @include('layouts.breadcrumb', ['linkPage' => url('users'), 'active' => 'List'])
@endsection
@section('content')
    <div class="w-100">
        <div class="card">
            <div class="card-body">
                <div class="w-100 d-flex justify-content-between mb-3">
                    <div class="">
                        <h3>Users</h3>
                    </div>
                    <div class="">
                        <a href="{{ url('users/create') }}" class="btn btn-primary btn-sm"><i
                                class="fas fa-plus"></i>&nbsp;Tambah</a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped display" id="datatables" style="width: 100%">
                        <thead>
                            <tr>
                                <th scope="col" width="30px">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Username</th>
                                <th scope="col">Role</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $item)
                                <tr>
                                    <th>{{ $loop->iteration }}</th>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->username }}</td>
                                    <td>
                                        @if ($item->role_id == 1)
                                            Superadmin
                                        @else
                                            Mahasiswa
                                        @endif
                                    </td>
                                    <td>
                                        @if ($item->role_id != 1)
                                            <form action="{{ url('users/' . enc($item->id)) }}" method="POST">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" class="btn btn-danger btn-sm"><i
                                                        class="fas fa-trash"></i></button>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $('#datatables').DataTable();
    </script>
@endsection
