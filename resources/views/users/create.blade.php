@extends('layouts.layout')
@section('breadcrumb')
    @include('layouts.breadcrumb', ['linkPage' => url('users'), 'active' => 'Add'])
@endsection
@section('content')
    <div class="w-100">
        <div class="card">
            <form action="{{ url('users') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="username">Username</label>
                                <input type="text" class="form-control" id="username" placeholder="Masukkan Username"
                                    name="username" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Nama Lengkap</label>
                                <input type="text" class="form-control" id="name"
                                    placeholder="Masukkan Nama Lengkap" required name="name">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="text" class="form-control" id="email" placeholder="Masukkan Email"
                                    name="email" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="role">Role</label>
                                <select class="form-control" id="role" name="role" required>
                                    <option value="" selected>-- pilih --</option>
                                    @foreach ($roles as $item)
                                        <option value="{{ $item }}">{{ $item }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="show-fakultas" style="display: none;">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="fakultas">Fakultas</label>
                                <select class="form-control" id="fakultas" name="fakultas_id" required>
                                    <option value="" selected>-- pilih --</option>
                                    @foreach ($fakultas as $item)
                                        <option value="{{ $item->id }}">{{ $item->fakultas_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="prodi">Prodi</label>
                                <select class="form-control" id="prodi" name="prodi_id" required>
                                    <option value="" selected>-- pilih --</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" id="password" placeholder="Masukkan Password"
                                    name="password" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="password">Konfirmasi Password</label>
                                <input type="password" class="form-control" id="password"
                                    placeholder="Masukkan Konfirmasi Password" name="konfirmasi_password" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="foto">Profile Photo</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="customFile" name="foto">
                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer w-100 d-flex justify-content-between">
                    <div class="">
                        <a href="{{ url()->previous() }}" class="btn btn-default">Back</a>
                    </div>
                    <div class="">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $('.custom-file-input').on('change', function() {
            let fileName = $(this).val().split('\\').pop();
            $(this).next('.custom-file-label').addClass("selected").html(fileName);
        });

        $('#role').change(function() {
            if ($(this).val() == 'Prodi') {
                $('#show-fakultas').show();
            } else {
                $('#show-fakultas').hide();
            }
        })

        $('#fakultas').change(function() {
            // console.log($(this).val());
            if ($(this).val() != '') {
                $.ajax(`/user/fakultas/${$(this).val()}`, {
                    success: function(data, status, xhr) {
                        // console.log(data)
                        let html = '<option value="" selected>-- pilih --</option>';
                        $.each(data, function(i, val) {
                            html += `<option value="${val.id}">${val.prodi_nama}</option>`;
                        })

                        $('#prodi').html(html);
                    }
                })
            }
        })
    </script>
    @if ($errors->any())
        <script>
            swal({
                icon: "error",
                title: "Add User Gagal",
                text: "{{ $errors->first() }}",
                buttons: false
            });
        </script>
        {{-- $errors->first() --}}
    @endif
@endsection
