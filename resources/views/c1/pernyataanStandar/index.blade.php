@extends('layouts.layout')
@section('breadcrumb')
    @include('layouts.breadcrumb', ['linkPage' => url('c1/pernyataan-standard'), 'active' => 'List'])
@endsection
@section('catatan')
    <p class="text-white text-justify">UPPS memiliki:
        1) visi yang mencerminkan visi perguruan tinggi dan memayungi visi keilmuan terkait keunikan program studi serta
        didukung data konsistensi implementasinya, 2) misi, tujuan, dan strategi yang searah dan bersinerji dengan misi,
        tujuan, dan strategi perguruan tinggi serta mendukung pengembangan program studi dengan data konsistensi
        implementasinya.</p>
@endsection
@section('content')
    <div class="w-100">
        <div class="card">
            <div class="card-body">
                <ul class="nav nav-tabs mb-3">
                    <li class="nav-item">
                        <a class="nav-link active" id="indikatorUtama-tab" data-toggle="tab" href="#indikatorUtama"
                            role="tab" aria-controls="indikatorUtama" aria-selected="true">Indikator Kinerja Utama</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="indikatorTambahan-tab" data-toggle="tab" href="#indikatorTambahan"
                            role="tab" aria-controls="indikatorTambahan" aria-selected="true">Indikator Kinerja
                            Tambahan</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="strategi-tab" data-toggle="tab" href="#strategi" role="tab"
                            aria-controls="strategi" aria-selected="true">Strategi Pencapaian</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="realisasi-tab" data-toggle="tab" href="#realisasi" role="tab"
                            aria-controls="realisasi" aria-selected="true">Realisasi Pencapaian</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="faktorPendukung-tab" data-toggle="tab" href="#faktorPendukung"
                            role="tab" aria-controls="faktorPendukung" aria-selected="true">Faktor Pendukung
                            Keberhasilan</a>
                    </li>
                </ul>
                <div class="tab-content mb-3" id="pills-tabContent">
                    <div class="tab-pane active" id="indikatorUtama" role="tabpanel" aria-labelledby="indikatorUtama-tab">
                        @include('c1.pernyataanStandar.indikatorUtama')
                    </div>
                    <div class="tab-pane" id="indikatorTambahan" role="tabpanel" aria-labelledby="indikatorTambahan-tab">
                        @include('c1.pernyataanStandar.indikatorTambahan')
                    </div>
                    <div class="tab-pane" id="strategi" role="tabpanel" aria-labelledby="strategi-tab">
                        @include('c1.pernyataanStandar.strategiPencapaian')
                    </div>
                    <div class="tab-pane" id="realisasi" role="tabpanel" aria-labelledby="realisasi-tab">
                        @include('c1.pernyataanStandar.realisasiPencapaian')
                    </div>
                    <div class="tab-pane" id="faktorPendukung" role="tabpanel" aria-labelledby="faktorPendukung-tab">
                        @include('c1.pernyataanStandar.faktorPendukung')
                    </div>
                </div>
            </div>
            @if ($hasilAudit)
                @if (!in_array($hasilAudit->status, ['temuan', 'tindak lanjut']))
                    <div class="form-group mb-3">
                        <label for="tanggal">Rencana Tanggal Pemenuhan</label><br>
                        <input type="date" name="tanggal_pemenuhan" id="tanggal" class="form-control"
                            value="{{ $hasilAudit->tanggal_pemenuhan }}">
                    </div>
                @endif
            @endif
            @if ($hasilAudit)
                @if (in_array($hasilAudit->status, ['temuan', 'tindak lanjut']))
                @else
                    <div class="card-footer">
                        <button class="btn btn-primary float-right" type="button"
                            onclick="handleTindakLanjut()">Kirim</button>
                    </div>
                @endif
            @else
                <div class="card-footer">
                    <button class="btn btn-primary float-right" type="button" onclick="handleKonfirmasi()">Kirim</button>
                </div>
            @endif
        </div>
    </div>
@endsection
@section('js')
    <script type="text/javascript">
        const fakultas = '{{ Auth::user()->fakultas_id }}';
        const prodi = '{{ Auth::user()->prodi_id }}';

        window.livewire.on('indikatorUtama', () => {
            $('#indikatorutama').modal('hide');
            $('#indikatorutamaedit').modal('hide');
        });

        function handleTindakLanjut() {
            swal({
                title: 'Yakin kirim?',
                text: "Tekan tombol yakin jika file sudah lengkap !!!",
                type: 'warning',
                buttons: {
                    confirm: {
                        text: 'Yakin',
                        className: 'btn btn-success'
                    },
                    cancel: {
                        visible: true,
                        className: 'btn btn-danger'
                    }
                }
            }).then((Delete) => {
                if (Delete) {
                    window.location =
                        `/c1/pernyataan-standard/tindak_lanjut?fakultas_id=${fakultas}&prodi_id=${prodi}&tanggal=${$('#tanggal').val()}`;
                } else {
                    swal.close();
                }
            });
        }

        function handleKonfirmasi() {
            swal({
                title: 'Yakin kirim?',
                text: "Tekan tombol yakin jika file sudah lengkap !!!",
                type: 'warning',
                buttons: {
                    confirm: {
                        text: 'Yakin',
                        className: 'btn btn-success'
                    },
                    cancel: {
                        visible: true,
                        className: 'btn btn-danger'
                    }
                }
            }).then((Delete) => {
                if (Delete) {
                    window.location =
                        `/c1/pernyataan-standard/kirim_berkas?fakultas_id=${fakultas}&prodi_id=${prodi}`;
                } else {
                    swal.close();
                }
            });
        }
    </script>
@endsection
