@extends('layouts.layout')
@section('breadcrumb')
    @include('layouts.breadcrumb', ['linkPage' => url('c1/temuan'), 'active' => 'List'])
@endsection
@section('content')
    <div class="card">
        <div class="card-body">
            <h2>Nama File : {{ $data->dokumen_upload_nama }}</h2>
            <hr>
            <form action="{{ url('c1/temuan/' . enc($data->id)) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="form-group">
                    <label for="uraian">Uraian</label>
                    <textarea name="uraian" id="uraian" class="form-control" readonly>{{ $data->uraian }}</textarea>
                </div>
                <div class="form-group">
                    <label for="informasi">Informasi</label>
                    <textarea name="informasi" id="informasi" class="form-control" readonly>{{ $data->informasi }}</textarea>
                </div>
                <div class="form-group">
                    <label for="klasifikasi_id">Klasifikasi</label>
                    <select name="klasifikasi_id" id="klasifikasi_id" class="form-control" disabled>
                        @foreach ($klasifikasi as $item)
                            <option value="{{ $item }}"
                                {{ $loop->iteration == $data->klasifikasi_id ? 'selected' : '' }}>{{ $item }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="upload">Upload File Dokumen</label>
                    <input type="file" class="form-control" name="upload" required>
                </div>
                <button type="submit" class="btn btn-primary w-100">Kirim</button>
            </form>
        </div>
    </div>
@endsection
