@extends('layouts.layout')
@section('breadcrumb')
    @include('layouts.breadcrumb', ['linkPage' => url('c1/pernyataan-standard'), 'active' => 'List'])
@endsection
@section('catatan')
    <p class="text-white text-justify">Evaluasi dan pemutakhiran kurikulum secara berkala tiap 4 s.d. 5 tahun yang melibatkan
        pemangku kepentingan internal dan eksternal, serta direview oleh pakar bidang ilmu program studi, industri,
        asosiasi, serta sesuai perkembangan ipteks dan kebutuhan pengguna</p>
@endsection
