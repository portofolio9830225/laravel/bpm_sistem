<div class="d-flex justify-content-between mb-3">
    <h3>Strategi Pencapaian</h3>
    @if ($hasilAudit)
        @if (in_array($hasilAudit->status, ['temuan', 'tindak lanjut']))
        @else
            <div class="">
                <button class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#realisasiPencapaian"><i
                        class="fa fa-plus"></i>&nbsp;Tambah</button>
            </div>
        @endif
    @else
        <div class="">
            <button class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#realisasiPencapaian"><i
                    class="fa fa-plus"></i>&nbsp;Tambah</button>
        </div>
    @endif
</div>

<!-- Modal Tambah -->
<div class="modal fade" id="realisasiPencapaian" tabindex="-1" role="dialog"
    aria-labelledby="realisasiPencapaianLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="realisasiPencapaianLabel">Strategi Pencapaian</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ url('c1/pernyataan-standard') }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="dokumen_kode" value="Realisasi Pencapaian">
                <input type="hidden" name="sub_modul_nama" value="Pernyataan Standard">
                <div class="modal-body">
                    <input type="text" class="form-control mb-3" id="nama" placeholder="Nama"
                        name="dokumen_upload_nama" required>
                    <input type="file" class="form-control" name="file" accept=".pdf" required>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-sm btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="table-responsive w-100">
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col" width="30">#</th>
                <th scope="col">Nama</th>
                <th scope="col">File</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @if ($realisasiPencapaian)
                @foreach ($realisasiPencapaian->dokumen_upload as $item)
                    <tr>
                        <th>{{ $loop->iteration }}</th>
                        <td>{{ $item->dokumen_upload_nama }}</td>
                        <td><button class="btn btn-sm btn-outline-danger"
                                onclick="window.location='/c1/pernyataan-standard/download/{{ $item->id }}'">Download
                                File</button></td>
                        <td>
                            <form action="{{ url('c1/pernyataan-standard/' . enc($item->id)) }}" method="post"
                                onsubmit="return confirm('yakin delete')">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-danger btn-sm" data-toggle="tooltip"
                                    data-placement="top" title="Delete"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <th colspan="4" class="text-center">No data available</th>
                </tr>
            @endif
        </tbody>
    </table>
</div>
