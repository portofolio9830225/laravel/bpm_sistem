@extends('layouts.layout')
@section('breadcrumb')
    @include('layouts.breadcrumb', ['linkPage' => url('users'), 'active' => 'List'])
@endsection
@section('content')
    <div class="w-100">
        <div class="card">
            <div class="card-body">
                <div class="w-100 d-flex justify-content-between mb-3">
                    <div class="">
                        <h3>{{ $title }}</h3>
                    </div>
                    <div class="">
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal">
                            <i class="fas fa-plus"></i>&nbsp;Tambah
                        </button>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped display" id="datatables" style="width: 100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th scope="col" width="30px">No</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                                <tr>
                                    <th>{{ $loop->iteration }}</th>
                                    <td>{{ $item->prodi_nama }}</td>
                                    <td>
                                        <form action="{{ url('program-studi/' . enc($item->id)) }}" method="POST">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-danger btn-sm"><i
                                                    class="fas fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{ url('program-studi') }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nama Program Studi</label>
                            <input type="text" name="prodi_nama" class="form-control" placeholder="Nama Program Studi"
                                required>
                        </div>
                        <div class="form-group">
                            <label for="fakultas">Fakultas</label>
                            <select class="form-control" id="fakultas" required name="fakultas_id">
                                <option value="">-- Pilih --</option>
                                @foreach ($fakultas as $item)
                                    <option value="{{ $item->id }}">{{ $item->fakultas_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-sm btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $('#datatables').DataTable();
    </script>
@endsection
