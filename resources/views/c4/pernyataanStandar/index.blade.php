@extends('layouts.layout')
@section('breadcrumb')
    @include('layouts.breadcrumb', ['linkPage' => url('c1/pernyataan-standard'), 'active' => 'List'])
@endsection
@section('catatan')
    <p class="text-white text-justify">Jumlah dosen tetap yang ditugaskan sebagai pengampu mata kuliah dengan bidang keahlian
        yang sesuai dengan kompetensi inti program studi yang diakreditasi, ≥ 12 maka Skor = 4</p>
@endsection
