@extends('layouts.layout')
@section('breadcrumb')
    @include('layouts.breadcrumb', ['linkPage' => url('c1/pernyataan-standard'), 'active' => 'List'])
@endsection
@section('catatan')
    <p class="text-white text-justify">Rata-rata dana operasional pendidikan/mahasiswa/ tahun dalam 3 tahun terakhir</p>
@endsection
