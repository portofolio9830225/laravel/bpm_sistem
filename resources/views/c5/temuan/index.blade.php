@extends('layouts.layout')
@section('breadcrumb')
    @include('layouts.breadcrumb', ['linkPage' => url('c1/temuan'), 'active' => 'List'])
@endsection
@section('content')
    <div class="card">
        @if (in_array(Auth::user()->role_id, [1, 3]))
            <div class="card-body">

                <div class="w-100 pb-2 border-bottom mb-3">
                    <div class="row ">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="fakultas">Fakultas</label>
                                <select class="form-control" id="fakultas">
                                    <option value="">-- Pilih --</option>
                                    @foreach ($fakultas as $item)
                                        <option value="{{ $item->id }}">{{ $item->fakultas_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="prodi">Prodi</label>
                                <select class="form-control" id="prodi">
                                    <option value="">-- Pilih --</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="pt-4">
                                <button type="button" class="btn btn-primary" id="btnHasilAudit"><i
                                        class="fas fa-search"></i>&nbsp;Search</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="w-100 py-2 text-center" id="loading" style="display: none;">Loading...</div>
                <div class="accordion accordion-secondary" id="listPersyaratanStandard" style="display: none;">
                </div>

                <form action="" method="post" id="formTemuan" style="display: none;">
                    @csrf
                    <input type="hidden" name="fakultas_id" id="fakultas_idForm">
                    <input type="hidden" name="prodi_id" id="prodi_idForm">
                    <div class="form-group mb-3">
                        <label for="tanggal">Rencana Tanggal Pemenuhan</label><br>
                        <input type="date" name="tanggal_pemenuhan" id="tanggal" class="form-control" required>
                    </div>
                    <div class="pt-2 border-top">
                        <button type="submit" class="btn btn-primary float-right">Kirim</button>
                    </div>
                </form>
            </div>
        @elseif(Auth::user()->role_id == 4)
            <div class="card-body">
                @if ($cekHasilAudit)
                    @if ($cekHasilAudit->status == 'temuan')
                        <div class="w-100 bg-info text-center py-2 text-white">
                            Proses Pengecekan oleh Audit
                        </div>
                    @elseif($cekHasilAudit->status == 'selesai temuan')
                        @if (count($dokumen) > 0)
                            @foreach ($dokumen as $i => $item)
                                <div class="accordion accordion-secondary">
                                    <div class="card">
                                        <div class="card-header" id="heading{{ $i }}" data-toggle="collapse"
                                            data-target="#collapse{{ $i }}">
                                            <div class="span-title">{{ $item->dokumen_kode }}</div>
                                        </div>

                                        <div id="collapse{{ $i }}" class="collapse" data-parent="#accordion">
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table class="table table-hover table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col" width="50px">#</th>
                                                                <th scope="col">Nama</th>
                                                                <th scope="col">Tanggal Pemenuhan</th>
                                                                <th scope="col">Aksi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($item->dokumen_upload as $item2)
                                                                <tr>
                                                                    <th>{{ $loop->iteration }}</th>
                                                                    <td>{{ $item2->dokumen_upload_nama }}</td>
                                                                    <td>
                                                                        <input type="date" name="" id="">
                                                                    </td>
                                                                    <td>
                                                                        <button class="btn btn-info btn-sm"><i
                                                                                class="fa fa-eye"
                                                                                onclick="window.location='{{ url('c1/temuan/' . enc($item2->id) . '/edit') }}'"></i></button>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    @else
                        <a href="{{ url('c1/tindak-lanjut') }}" class="btn btn-primary w-100 text-center">
                            Silahkan Cek Menu Tindak Lanjut / Klik Disini
                        </a>
                    @endif
                @else
                    <div class="w-100 bg-info text-center py-2 text-white">
                        Silahkan Upload Dokumen
                    </div>
                @endif
            </div>
        @endif
    </div>
@endsection
@section('js')
    <script>
        const linkPost = "{{ url('c1/temuan/dokumen-upload') }}";

        $('#fakultas').change(function() {
            if ($(this).val() != '') {
                $.ajax(`/user/fakultas/${$(this).val()}`, {
                    success: function(data, status, xhr) {
                        // console.log(data)
                        let html = '<option value="" selected>-- Pilih --</option>';
                        $.each(data, function(i, val) {
                            html += `<option value="${val.id}">${val.prodi_nama}</option>`;
                        })

                        $('#prodi').html(html);
                    }
                })
            }
        })

        $('#btnHasilAudit').click(function() {
            if ($('#fakultas').val() == '') {
                alert('silahkan pilih fakultas');
            } else if ($('#prodi').val() == '') {
                alert('silahkan pilih prodi');
            } else {
                $('#loading').show();
                $.ajax(`/c1/temuan/file/${$('#fakultas').val()}/${$('#prodi').val()}`, {
                    success: function(data, status, xhr) {
                        // console.log(data);

                        if (data.status == 'temuan') {
                            if (data.data.length > 0) {
                                let html = '';
                                $.each(data.data, function(i, val) {
                                    html += '<div class="card">';
                                    html +=
                                        `<div class="card-header" data-toggle="collapse" data-target="#collapse${i}"><div class="span-title"><i class="fas fa-list"></i>&nbsp;${val.dokumen_kode}</div></div>`;

                                    html +=
                                        `<div id="collapse${i}" class="collapse" data-parent="#accordion"><div class="card-body">`;

                                    $.each(val.dokumen_upload, function(i, val2) {
                                        html +=
                                            `<div class="w-100 bg-white px-3 py-2 mb-3">`;
                                        html += `<div class="w-100 justify-content-between d-flex mb-3">
                                                    <span>${val2.dokumen_upload_nama}</span>
                                                    <div class="">
                                                        <button class="btn btn-sm btn-outline-danger">Download File</button>
                                                    </div>
                                                </div>`;

                                        html += `<form action="#" method="post" onsubmit="handleKirim(event)">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                <input type="hidden" name="dokumen_upload_id" value="${val2.id}">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <textarea class="form-control" id="uraian" name="uraian" placeholder="Uraian (Problem dan Lokasi)" required></textarea>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <textarea class="form-control" id="informasi" name="informasi" placeholder="Informasi/ Dokumen yang Tersedia" required></textarea>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <select class="form-control" id="klasifikasi_id" name="klasifikasi_id" required>
                                                            <option value="">-- pilih klasifikasi --</option>
                                                            <option value="Minor">Minor</option>
                                                            <option value="Mayor">Mayor</option>
                                                            <option value="Observasi">Observasi</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <button type="submit" class="w-100 btn btn-info"><i class="fas fa-save"></i>&nbsp;Submit</button>
                                                    </div>
                                                </div>
                                            </form></div>`;
                                    })

                                    html += `</div></div></div>`;
                                })

                                $('#loading').hide();
                                $('#listPersyaratanStandard').show().html(html);
                                $('#fakultas_idForm').val($('#fakultas').val());
                                $('#prodi_idForm').val($('#prodi').val());
                                $('#formTemuan').show().attr('action', '/c1/temuan');
                            } else {
                                alert('file tidak ada');
                                $('#loading').hide();
                            }
                        } else if (data.status == 'tindak lanjut') {
                            alert('sudah tahap tindak lanjut');
                            $('#loading').hide();
                        } else {
                            alert('silahkan cek kembali');
                            $('#loading').hide();
                        }
                    }
                })
            }
        })

        // onsubmit="handleKirim(event)" 
        function handleKirim(e) {
            e.preventDefault();
            let data = {
                _token: e.target[0].value,
                dokumen_upload_id: e.target[1].value,
                uraian: e.target[2].value,
                informasi: e.target[3].value,
                klasifikasi_id: e.target[4].value
            };

            e.target[5].innerHTML = 'Loading...';

            $.ajax({
                method: 'POST',
                url: `${linkPost}`,
                data: data,
                success: function(data) {
                    if (data.status == 'SUCCESS') {
                        alert('Data berhasil tersimpan');
                        e.target[5].innerHTML = '<i class="fas fa-save"></i>&nbsp;Submit';
                    }
                }
            });

            // console.log(data)
        }
    </script>
@endsection
