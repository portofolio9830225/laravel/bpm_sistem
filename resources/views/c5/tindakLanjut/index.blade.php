@extends('layouts.layout')
@section('breadcrumb')
    @include('layouts.breadcrumb', ['linkPage' => url('c1/tindak-lanjut'), 'active' => 'List'])
@endsection
@section('content')
    <div class="card">
        <div class="card-body">

            @if (in_array(Auth::user()->role_id, [1, 3]))

                <div class="w-100 pb-2 border-bottom mb-3">
                    <div class="row ">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="fakultas">Fakultas</label>
                                <select class="form-control" id="fakultas">
                                    <option value="">-- Pilih --</option>
                                    @foreach ($fakultas as $item)
                                        <option value="{{ $item->id }}">{{ $item->fakultas_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="prodi">Prodi</label>
                                <select class="form-control" id="prodi">
                                    <option value="">-- Pilih --</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="pt-4">
                                <button type="button" class="btn btn-primary" id="btnHasilAudit"><i
                                        class="fas fa-search"></i>&nbsp;Search</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="w-100 py-2 text-center" id="loading" style="display: none;">Loading...</div>
                <div class="accordion accordion-secondary" id="listPersyaratanStandard" style="display: none;"></div>

                <form action="" method="post" id="formTindakLanjut" style="display: none;">
                    @csrf
                    <input type="hidden" name="fakultas_id" id="fakultas_idForm">
                    <input type="hidden" name="prodi_id" id="prodi_idForm">
                    <div class="form-group">
                        <label for="akar">Akar Masalah</label>
                        <textarea class="form-control" name="akar_permasalahan" id="akar" rows="3"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="penanganan">Penanganan (Correction)</label>
                        <textarea class="form-control" name="penanganan" id="penanganan" rows="3"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="perbaikan">Perbaikan Akar Masalah (Correction Action)</label>
                        <textarea class="form-control" name="perbaikan_akar_masalah" id="perbaikan" rows="3"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="perbaikan">Nilai</label>
                        <select name="" id="" class="form-control">
                            <option value="">-- pilih --</option>
                            <option value="">4</option>
                            <option value="">3</option>
                            <option value="">2</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="perbaikan">Validasi</label>
                        <input type="text" readonly value="Auditor {{ Auth::user()->name }}" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-primary float-right">Kirim</button>
                </form>
            @else
                @if ($hasilAudit)
                    @if ($hasilAudit->status == 'tindak lanjut')
                        <div class="w-100 bg-info text-center py-2 text-white">
                            Proses Pengecekan oleh Audit
                        </div>
                    @else
                        <div class="w-100 bg-info text-center py-2 text-white">
                            Masih Tahap Proses Temuan
                        </div>
                    @endif
                @else
                    <div class="w-100 bg-info text-center py-2 text-white">
                        Silahkan Upload Dokumen
                    </div>
                @endif
            @endif
        </div>
    </div>
@endsection
@section('js')
    <script>
        $('#fakultas').change(function() {
            if ($(this).val() != '') {
                $.ajax(`/user/fakultas/${$(this).val()}`, {
                    success: function(data, status, xhr) {
                        // console.log(data)
                        let html = '<option value="" selected>-- Pilih --</option>';
                        $.each(data, function(i, val) {
                            html += `<option value="${val.id}">${val.prodi_nama}</option>`;
                        })

                        $('#prodi').html(html);
                    }
                })
            }
        })

        $('#btnHasilAudit').click(function() {
            if ($('#fakultas').val() == '') {
                alert('silahkan pilih fakultas');
            } else if ($('#prodi').val() == '') {
                alert('silahkan pilih prodi');
            } else {
                $('#loading').show();
                $.ajax(`/c1/temuan/file/${$('#fakultas').val()}/${$('#prodi').val()}`, {
                    success: function(data, status, xhr) {
                        // console.log(data);

                        if (data.status == 'tindak lanjut') {
                            if (data.data.length > 0) {
                                let html = '';
                                $.each(data.data, function(i, val) {
                                    html += '<div class="card">';
                                    html +=
                                        `<div class="card-header" data-toggle="collapse" data-target="#collapse${i}"><div class="span-title"><i class="fas fa-list"></i>&nbsp;${val.dokumen_kode}</div></div>`;

                                    html +=
                                        `<div id="collapse${i}" class="collapse" data-parent="#accordion"><div class="card-body"><ul class="list-group list-group-flush">`;

                                    $.each(val.dokumen_upload, function(i, val2) {
                                        html += `<li class="list-group-item">
                                    <div class="w-100 justify-content-between d-flex">
                                        <span>${val2.dokumen_upload_nama}</span>
                                        <div class="">
                                            <button class="btn btn-sm btn-outline-danger">Download File</button>
                                        </div>
                                    </div>
                                </li>`
                                    })

                                    html += `</ul></div></div></div>`;
                                })

                                $('#loading').hide();
                                $('#listPersyaratanStandard').show().html(html);
                                $('#fakultas_idForm').val($('#fakultas').val());
                                $('#prodi_idForm').val($('#prodi').val());
                                $('#formTindakLanjut').show().attr('action', '/c1/tindak-lanjut');
                            } else {
                                alert('file tidak ada');
                                $('#loading').hide();
                            }
                        } else if (data.status == 'temuan') {
                            alert('masih tahap temuan');
                            $('#loading').hide();
                        } else {
                            alert('silahkan cek kembali');
                            $('#loading').hide();
                        }
                    }
                })
            }
        })
    </script>
@endsection
