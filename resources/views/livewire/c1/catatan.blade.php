<div>
    {{-- The Master doesn't talk, he acts. --}}
    {{-- <form action="{{ url('c1/pernyataan-standard/catatan') }}" method="post"> --}}
    {{-- @csrf --}}
    <input type="hidden" name="dokumen_upload" value="Indikator Kinerja Utama">
    <div class="w-100 mb-3">
        <div class="form-group">
            <label for="exampleInputEmail1">Catatan 2 {{ $title }}</label>
            <textarea name="catatan" class="form-control mb-2" rows="2"></textarea>
        </div>
        <button type="submit" class="btn btn-primary w-100">Kirim</button>
    </div>
    {{-- </form> --}}
</div>
