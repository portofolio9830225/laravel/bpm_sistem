<div>
    <div class="d-flex justify-content-between mb-3">
        <h3>Indikator Kinerja Utama</h3>
        <div class="">
            <button class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#indikatorutama"><i
                    class="fa fa-plus"></i>&nbsp;Tambah</button>
        </div>
    </div>

    <!-- Modal Tambah -->
    <div wire:ignore.self class="modal fade" id="indikatorutama" tabindex="-1" role="dialog"
        aria-labelledby="indikatorutamaLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="indikatorutamaLabel">Indikator Kinerja Utama</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" wire:submit.prevent="store" enctype="multipart/form-data">
                    <div class="modal-body">
                        <input type="text" class="form-control mb-3" id="nama" placeholder="Nama"
                            wire:model="dokumen_nama" required>
                        <input type="file" class="form-control" wire:model="file" accept=".pdf">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-sm btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th width="30">#</th>
                    <th>Nama</th>
                    <th>File</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @if (count($data) > 0)
                    @foreach ($data as $item)
                        <tr>
                            <th width="30">{{ $loop->iteration }}</th>
                            <td>{{ $item->dokumen_nama }}</td>
                            <td>
                                <button onclick="window.location='/c1/pernyataan-standard/review/{{ $item->id }}'"
                                    class="btn btn-sm btn-outline-danger">Download File</button>
                            </td>
                            <td>
                                <button class="btn btn-info btn-sm" wire:click="edit({{ $item->id }})"
                                    data-toggle="modal" data-target="#indikatorutamaedit"><i
                                        class="fa fa-edit"></i></button>
                                <button class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top"
                                    title="Delete" wire:click="delete({{ $item->id }})"><i
                                        class="fa fa-trash"></i></button>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <th colspan="4" class="text-center">No data available</th>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>

    <!-- Modal Edit -->
    <div wire:ignore.self class="modal fade" id="indikatorutamaedit" tabindex="-1" role="dialog"
        aria-labelledby="indikatorutamaeditLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="indikatorutamaeditLabel">Indikator Kinerja Utama</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" wire:submit.prevent="update">
                    <div class="modal-body">
                        <input type="text" class="form-control mb-3" id="nama" placeholder="Nama"
                            wire:model="dokumen_namaEdit" required>
                        {{-- <div class="custom-file">
                        <input type="file" class="custom-file-input" id="customFile"
                            onchange="this.nextElementSibling.innerText = this.files[0].name" required accept=".pdf">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div> --}}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-sm btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
