<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="https://bpm.usm.ac.id/simutujaya/homepage/assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="https://bpm.usm.ac.id/simutujaya/homepage/assets/img/logo.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>

        SIPMU Jaya
    </title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <!-- Extra details for Live View on GitHub Pages -->

    <!-- Canonical SEO -->
    <link rel="canonical" href="https://www.creative-tim.com/product/material-kit" />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="{{ asset('css/material-kit.css') }}" rel="stylesheet" />

    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="{{ asset('demo.css') }}" rel="stylesheet" />



    <!-- CSRF Token -->
    <meta name="csrf-token" content="t8oMANzkiRQ6Vi5nWxHVqd4UxSZ7Cea173buUaBA">





</head>

<body class="landing-page sidebar-collapse">
    <nav class="navbar navbar-transparent navbar-color-on-scroll fixed-top navbar-expand-lg" color-on-scroll="100"
        id="sectionsNav">
        <div class="container">
            <div class="navbar-translate">
                <!--         <div class="col-lg-4  ">
        <div class="avatar">
                <img src="https://bpm.usm.ac.id/simutujaya/homepage/assets/img/uki.png" alt="Circle Image" class="img-raised rounded-circle img-fluid">
        </div>
        </div> -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="navbar-toggler-icon"></span>
                    <span class="navbar-toggler-icon"></span>
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="navbar-nav ml-auto">



                    <li class="nav-item">
                        <a class="nav-link text-dark" href="javascript:void(0)" onclick="scrollToDownload()">
                            Login / Register
                        </a>
                    </li>

                </ul>
            </div>
        </div>
    </nav>
    <div class="page-header" data-parallax="true"
        style="background-image: url(https://bpm.usm.ac.id/simutujaya/homepage/assets/img/USM.png )">
        <div class="container">
            <div class="row">
                <div class="col-md-11 text-left ml-0 mr-5">
                    <h3 class="title  text-dark">Welcome To</h3>

                    <h1 class="title  text-dark">Sistem Informasi Penjaminan Mutu USM Jaya</h1>
                    <h3 class="title  text-dark">BADAN PENJAMINAN MUTU</h3>
                    <br>
                </div>
            </div>
        </div>
    </div>
    <div class="main main-raised">
        <div class="container">
            <div class="section text-center">
                <div class="section section-download" id="downloadSection">
                    <div class="row">
                        <div class="col-md-6 ml-auto mr-auto">
                            <div class="profile-tabs">
                                <ul class="nav nav-pills nav-pills-icons justify-content-center " role="tablist">

                                    <li class="nav-item ">
                                        <a class="nav-link active bg-primary" style="background-color: #1A237E"
                                            href="javascript:;" role="tab" data-toggle="tab">
                                            <i class="material-icons">login</i> Login
                                        </a>
                                    </li>

                                    <!-- <li class="nav-item">
                                                    <a class="nav-link bg-danger" href="#regis" role="tab" data-toggle="tab">
                                                    <i class="material-icons">app_registration</i> Register
                                                  </a>
                                                </li>
                                 -->
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="tab-content tab-space">
                        <div class="tab-pane active text-center gallery" id="studio">
                            <br>


                            <div class="row">
                                <div class="col-lg-6 col-md-6 ml-auto mr-auto">
                                    <div class="card card-login">
                                        <form class="form" method="POST" action="{{ url('login') }}">
                                            @csrf
                                            <div class="card-header card-header-primary text-center">
                                                <h5 class="card-title">Selamat Datang di aplikasi SIPMU Jaya </h5>
                                                <p>Silahkan Login</p>

                                            </div>

                                            <div class="card-body"><br>

                                                @if (session()->has('error'))
                                                    <div class="alert alert-danger">{{ session('error') }}</div>
                                                @endif

                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="material-icons">mail</i>
                                                        </span>
                                                    </div>
                                                    <input id="email" type="email" class="form-control "
                                                        name="email" value="" required autocomplete="email"
                                                        placeholder="Masukkan email" required>
                                                </div>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="material-icons">lock_outline</i>
                                                        </span>
                                                    </div>
                                                    <input id="password" type="password" class="form-control "
                                                        name="password" required autocomplete="current-password"
                                                        placeholder="Masukkan password" required>

                                                </div>


                                            </div>
                                            <div class="footer text-left"><br>
                                                <br><br>
                                                <div class="col-md-auto  offset-md-8">
                                                    <button type="submit" class="btn btn-primary">
                                                        Login
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane  text-center gallery" id="regis">

                            <br>
                            <div class="row">
                                <div class="col-md-8   ml-auto mr-auto">
                                    <div class="card card-login">
                                        <form class="form" method="POST"
                                            action="https://bpm.usm.ac.id/simutujaya/register">
                                            <input type="hidden" name="_token"
                                                value="t8oMANzkiRQ6Vi5nWxHVqd4UxSZ7Cea173buUaBA">
                                            <div class="card-header card-header-danger text-center">
                                                <h5 class="card-title">Register</h5>
                                                <p3>Silahkan mendaftar dahulu</p3>
                                            </div>

                                            <div class="card-body"><br>

                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="material-icons">person</i>
                                                        </span>
                                                    </div>

                                                    <input id="name" type="text" class="form-control "
                                                        name="name" placeholder="masukkan nama" value=""
                                                        required autocomplete="name" autofocus>

                                                </div>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="material-icons">mail</i>
                                                        </span>
                                                    </div>
                                                    <input id="email" type="email" class="form-control "
                                                        placeholder="Maukkan email" name="email" value=""
                                                        required autocomplete="email">

                                                </div>

                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="material-icons">lock_outline</i>
                                                        </span>
                                                    </div>
                                                    <input id="password" type="password" class="form-control "
                                                        placeholder="masukkan password" name="password" required
                                                        autocomplete="new-password">

                                                </div>

                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="material-icons">lock</i>
                                                        </span>
                                                    </div>
                                                    <input id="password-confirm" type="password" class="form-control"
                                                        placeholder="masukkan ulang password"
                                                        name="password_confirmation" required
                                                        autocomplete="new-password">
                                                </div>
                                                <br>
                                                <div class="form-group  row mb-10">
                                                    <div class="col-md-6 offset-md-1">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <b class="text-danger">Prodi :</b>
                                                            </span>
                                                        </div>

                                                        <select class="form-control" name="prodi">
                                                            <option value="1">Teknik Informatika
                                                            </option>
                                                            <option value="2">Sistem Informasi
                                                            </option>
                                                            <option value="3">Manajemen
                                                            </option>
                                                            <option value="4">Akutansi
                                                            </option>
                                                            <option value="5">Teknik Sipil
                                                            </option>
                                                            <option value="6">Teknik Elektro
                                                            </option>
                                                            <option value="7">Ilmu Hukum
                                                            </option>
                                                            <option value="8">Manajemen Perusahaan
                                                            </option>
                                                            <option value="9">PWK
                                                            </option>
                                                            <option value="10">THP
                                                            </option>
                                                            <option value="11">Psikologi
                                                            </option>
                                                            <option value="12">Ilmu Komunikasi
                                                            </option>
                                                            <option value="13">Pariwisata
                                                            </option>
                                                            <option value="14">Hukum
                                                            </option>
                                                            <option value="15">Manajemen
                                                            </option>
                                                            <option value="16">Psikologi
                                                            </option>
                                                            <option value="0">testing
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group  row mb-10">

                                                        <div class="col-md-12 offset-md-9">
                                                            <div class="input-group-prepend">

                                                                <span class="input-group-text">

                                                                    <b class="text-danger">Fungsi :</b>

                                                                </span>
                                                            </div>


                                                            <select name="role" class="form-control">

                                                                <option value="admin">Admin</option>

                                                                <option value="satgas">Ka. Progdi</option>
                                                                <option value="GugusMutu">GugusMutu</option>
                                                                <option value="auditor">auditor</option>

                                                            </select>

                                                        </div>
                                                    </div>

                                                </div>
                                                <br>

                                                <div class="footer text-center">

                                                    <div class="form-group row mb-4">
                                                        <div class="col-md-6 offset-md-6">
                                                            <button type="submit" class="btn btn-danger">
                                                                Register
                                                            </button>
                                                        </div>
                                                    </div>

                                                </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>
                    <div class="copyright float-center">
                        Badan Penjaminan Mutu Universitas Semarang
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <footer class="footer footer-default">
        <div class="container">


        </div>
    </footer>



    <!--   Core JS Files   -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-material-design.min.js') }}"></script>
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datetimepicker.js') }}"></script>
    <script src="{{ asset('js/nouislider.min.js') }}"></script>
    <script src="{{ asset('js/material-kit.min.js') }}"></script>

    <script>
        // Facebook Pixel Code Don't Delete
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window,
            document, 'script', '//connect.facebook.net/en_US/fbevents.js');

        try {
            fbq('init', '111649226022273');
            fbq('track', "PageView");

        } catch (err) {
            console.log('Facebook Track Error:', err);
        }
    </script>
    <noscript>
        <img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=111649226022273&ev=PageView&noscript=1" />
    </noscript>
    <script>
        $(document).ready(function() {
            if ($('.datetimepicker').length != 0) {
                //init DateTimePickers
                materialKit.initFormExtendedDatetimepickers();
            }
            if ($('.slider').length != 0) {
                // Sliders Init
                materialKit.initSliders();
            }
        });


        function scrollToDownload() {
            if ($('.section-download').length != 0) {
                $("html, body").animate({
                    scrollTop: $('.section-download').offset().top
                }, 1000);
            }
        }
    </script>




</body>

</html>
