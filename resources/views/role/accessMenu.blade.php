@extends('layouts.layout')
@section('breadcrumb')
    @include('layouts.breadcrumb', ['linkPage' => url('role'), 'active' => 'Access Menu'])
@endsection
@section('content')
    <form action="{{ url('role/access-menu/' . enc($role->id)) }}" method="post">
        @csrf
        @method('put')
        <div class="w-100">
            <div class="card">
                <div class="card-body">
                    <h1>Role : {{ str($role->name)->upper() }}</h1>
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="30px">#</th>
                                <th>Nama Menu</th>
                                <th class="text-center">Active</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($access_menu as $item)
                                <tr>
                                    <th width="40">{{ $loop->iteration }}</th>
                                    <td>{{ $item['name_menu'] }}</td>
                                    <td class="text-center">
                                        <input type="checkbox" {{ $item['is_active'] == 1 ? 'checked' : '' }}
                                            name="permission[]" value="{{ $item['permission'] . '-view' }}">
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    <button type="button" onclick="document.location='{{ url()->previous() }}'"
                        class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </form>
@endsection
