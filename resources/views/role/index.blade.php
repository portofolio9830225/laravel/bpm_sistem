@extends('layouts.layout')
@section('breadcrumb')
    @include('layouts.breadcrumb', ['linkPage' => url('role'), 'active' => 'List'])
@endsection
@section('content')
    <div class="w-100">
        <div class="card">
            <div class="card-body">
                <div class="w-100 d-flex justify-content-between mb-3">
                    <div class="">
                        <h3>{{ $title }}</h3>
                    </div>
                    <div class="">
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal">
                            <i class="fas fa-plus"></i>&nbsp;Tambah
                        </button>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped display" id="datatables" style="width: 100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th scope="col" width="30px">No</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                                <tr>
                                    <th>{{ $loop->iteration }}</th>
                                    <td>{{ $item->name }}</td>
                                    <td>
                                        <div class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top"
                                            title="Access Menu"
                                            onclick="window.location='{{ url('role/access-menu/' . enc($item->id)) }}'"><i
                                                class="fas fa-cog"></i></div>
                                        @if ($item->id != 1)
                                            <form action="{{ url('role/' . enc($item->id)) }}" method="POST"
                                                class="d-inline">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" class="btn btn-danger btn-sm" data-toggle="tooltip"
                                                    data-placement="top" title="Delete"><i
                                                        class="fas fa-trash"></i></button>
                                            </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{ url('role') }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nama Role</label>
                            <input type="text" name="name" class="form-control" placeholder="Nama Role" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-sm btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $('#datatables').DataTable();
    </script>
@endsection
