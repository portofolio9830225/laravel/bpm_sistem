<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAclMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acl_menu', function (Blueprint $table) {
            $table->id();
            $table->string('name_menu')->nullable();
            $table->string('link')->nullable();
            $table->boolean('is_parent')->default(1);
            $table->integer('parent_id')->nullable();
            $table->string('parent_icon')->nullable();
            $table->string('permisson_key')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_admin');
    }
}
