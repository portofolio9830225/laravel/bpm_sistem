<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dokumen_upload', function (Blueprint $table) {
            $table->id();
            $table->integer('dokumen_id')->nullable();
            $table->string('dokumen_upload_nama')->nullable();
            $table->string('file')->nullable();
            $table->text('uraian')->nullable();
            $table->text('informasi')->nullable();
            $table->integer('klasifikasi_id')->nullable();
            $table->text('akar_permasalahan')->nullable();
            $table->text('penanganan')->nullable();
            $table->text('perbaikan_akar_masalah')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dokumen_upload');
    }
};
