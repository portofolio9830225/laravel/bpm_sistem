<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('header_jadwal', function (Blueprint $table) {
            $table->id();
            $table->integer('users_id')->nullable();
            $table->integer('fakultas_id')->nullable();
            $table->integer('prodi_id')->nullable();
            $table->integer('dokumen_id')->nullable();
            $table->integer('dokumen_upload_id')->nullable();
            $table->integer('header_jadwal_tanggal')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('header_jadwal');
    }
};
