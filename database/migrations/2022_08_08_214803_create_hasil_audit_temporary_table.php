<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hasil_audit_temporary', function (Blueprint $table) {
            $table->id();
            $table->text('akar_permasalahan')->nullable();
            $table->text('penanganan')->nullable();
            $table->text('perbaikan_akar_masalah')->nullable();
            $table->string('tanggal_pemenuhan')->nullable();
            $table->integer('klasifikasi_id')->nullable();
            $table->string('status')->nullable();
            $table->integer('fakultas_id')->nullable();
            $table->integer('prodi_id')->nullable();
            $table->integer('penilaian_id')->nullable();
            $table->text('uraian')->nullable();
            $table->text('informasi')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hasil_audit_temporary');
    }
};
