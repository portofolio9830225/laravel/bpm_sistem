<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hasil_audit', function (Blueprint $table) {
            $table->id();
            $table->integer('jadwal_audit_detail_id')->nullable();
            $table->integer('hasil_audit_ke')->nullable();
            $table->integer('penilaian_id')->nullable();
            $table->text('akar_permasalahan')->nullable();
            $table->text('penanganan')->nullable();
            $table->text('perbaikan_akar_masalah')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hasil_audit');
    }
};
