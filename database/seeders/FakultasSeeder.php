<?php

namespace Database\Seeders;

use App\Models\Fakultas;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class FakultasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fakultas = ['Fakultas Hukum', 'Fakultas Teknik', 'Fakultas Ekonomi', 'Fakultas Teknologi Pertanian', 'Fakultas Teknologi Informasi dan Komunikasi', 'Fakultas Psikologi', 'Pasca Sarjana'];

        foreach ($fakultas as $item) {
            Fakultas::create([
                'fakultas_name' => $item
            ]);
        }
    }
}
