<?php

namespace Database\Seeders;

use App\Models\Penilaian;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class PenilaianSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $penilaian = [
            [
                'penilaian_nilai' => 4,
                'penilaian_status' => 'Unggul',
            ],
            [
                'penilaian_nilai' => 3,
                'penilaian_status' => 'Baik Sekali',
            ],
            [
                'penilaian_nilai' => 2,
                'penilaian_status' => 'Baik'
            ]
        ];

        foreach ($penilaian as $item) {
            Penilaian::create($item);
        }
    }
}
