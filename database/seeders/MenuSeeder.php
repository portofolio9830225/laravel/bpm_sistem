<?php

namespace Database\Seeders;

use App\Models\Menu;
use App\Models\Modul;
use App\Models\SubModul;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;
use Illuminate\Support\Arr;
use Str;
use Spatie\Permission\Models\Permission;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = [
            [
                'name_menu'     => 'Home',
                'link'          => 'home',
                'is_parent'     => true,
                'permisson_key' => 'home'
            ],
            [
                'name_menu'     => 'Master',
                'link'          => '#',
                'is_parent'     => true,
                'sub_menu'      => [
                    [
                        'name_menu'     => 'Users',
                        'link'          => 'users',
                        'is_parent'     => false,
                        'permisson_key' => 'users'
                    ],
                    [
                        'name_menu'     => 'Fakultas',
                        'link'          => 'fakultas',
                        'is_parent'     => false,
                        'permisson_key' => 'fakultas'
                    ],
                    [
                        'name_menu'     => 'Program Studi',
                        'link'          => 'program-studi',
                        'is_parent'     => false,
                        'permisson_key' => 'program-studi'
                    ],
                ]
            ],
            [
                'name_menu'     => 'Menu',
                'link'          => 'menu',
                'is_parent'     => true,
                'permisson_key' => 'menu'
            ],
            [
                'name_menu'     => 'Role',
                'link'          => 'role',
                'is_parent'     => true,
                'permisson_key' => 'role'
            ],
            [
                'name_menu'     => 'Jadwal',
                'link'          => 'jadwal',
                'is_parent'     => true,
                'permisson_key' => 'jadwal'
            ],
            [
                'name_menu'     => 'Dokumen',
                'link'          => 'dokumen',
                'is_parent'     => true,
                'permisson_key' => 'dokumen'
            ],
            [
                'name_menu'     => 'C1',
                'link'          => '#',
                'is_parent'     => true,
                'sub_menu'      => [
                    [
                        'name_menu'     => 'Pernyataan Standard',
                        'link'          => 'c1/pernyataan-standard',
                        'is_parent'     => false,
                        'permisson_key' => 'c1-pernyataan-standard'
                    ],
                    [
                        'name_menu'     => 'Temuan',
                        'link'          => 'c1/temuan',
                        'is_parent'     => false,
                        'permisson_key' => 'c1-temuan'
                    ],
                    [
                        'name_menu'     => 'Tindak Lanjut',
                        'link'          => 'c1/tindak-lanjut',
                        'is_parent'     => false,
                        'permisson_key' => 'c1-tindak-lanjut'
                    ],
                ]
            ],
            [
                'name_menu'     => 'C2',
                'link'          => '#',
                'is_parent'     => true,
                'sub_menu'      => [
                    [
                        'name_menu'     => 'Pernyataan Standard',
                        'link'          => 'c2/pernyataan-standard',
                        'is_parent'     => false,
                        'permisson_key' => 'c2-pernyataan-standard'
                    ],
                    [
                        'name_menu'     => 'Temuan',
                        'link'          => 'c2/temuan',
                        'is_parent'     => false,
                        'permisson_key' => 'c2-temuan'
                    ],
                    [
                        'name_menu'     => 'Tindak Lanjut',
                        'link'          => 'c2/tindak-lanjut',
                        'is_parent'     => false,
                        'permisson_key' => 'c2-tindak-lanjut'
                    ],
                ]
            ],
            [
                'name_menu'     => 'C3',
                'link'          => '#',
                'is_parent'     => true,
                'sub_menu'      => [
                    [
                        'name_menu'     => 'Pernyataan Standard',
                        'link'          => 'c3/pernyataan-standard',
                        'is_parent'     => false,
                        'permisson_key' => 'c3-pernyataan-standard'
                    ],
                    [
                        'name_menu'     => 'Temuan',
                        'link'          => 'c3/temuan',
                        'is_parent'     => false,
                        'permisson_key' => 'c3-temuan'
                    ],
                    [
                        'name_menu'     => 'Tindak Lanjut',
                        'link'          => 'c3/tindak-lanjut',
                        'is_parent'     => false,
                        'permisson_key' => 'c3-tindak-lanjut'
                    ],
                ]
            ],
            [
                'name_menu'     => 'C4',
                'link'          => '#',
                'is_parent'     => true,
                'sub_menu'      => [
                    [
                        'name_menu'     => 'Pernyataan Standard',
                        'link'          => 'c4/pernyataan-standard',
                        'is_parent'     => false,
                        'permisson_key' => 'c4-pernyataan-standard'
                    ],
                    [
                        'name_menu'     => 'Temuan',
                        'link'          => 'c4/temuan',
                        'is_parent'     => false,
                        'permisson_key' => 'c4-temuan'
                    ],
                    [
                        'name_menu'     => 'Tindak Lanjut',
                        'link'          => 'c4/tindak-lanjut',
                        'is_parent'     => false,
                        'permisson_key' => 'c4-tindak-lanjut'
                    ],
                ]
            ],
            [
                'name_menu'     => 'C5',
                'link'          => '#',
                'is_parent'     => true,
                'sub_menu'      => [
                    [
                        'name_menu'     => 'Pernyataan Standard',
                        'link'          => 'c5/pernyataan-standard',
                        'is_parent'     => false,
                        'permisson_key' => 'c5-pernyataan-standard'
                    ],
                    [
                        'name_menu'     => 'Temuan',
                        'link'          => 'c5/temuan',
                        'is_parent'     => false,
                        'permisson_key' => 'c5-temuan'
                    ],
                    [
                        'name_menu'     => 'Tindak Lanjut',
                        'link'          => 'c5/tindak-lanjut',
                        'is_parent'     => false,
                        'permisson_key' => 'c5-tindak-lanjut'
                    ],
                ]
            ],
            [
                'name_menu'     => 'C6',
                'link'          => '#',
                'is_parent'     => true,
                'sub_menu'      => [
                    [
                        'name_menu'     => 'Pernyataan Standard',
                        'link'          => 'c6/pernyataan-standard',
                        'is_parent'     => false,
                        'permisson_key' => 'c6-pernyataan-standard'
                    ],
                    [
                        'name_menu'     => 'Temuan',
                        'link'          => 'c6/temuan',
                        'is_parent'     => false,
                        'permisson_key' => 'c6-temuan'
                    ],
                    [
                        'name_menu'     => 'Tindak Lanjut',
                        'link'          => 'c6/tindak-lanjut',
                        'is_parent'     => false,
                        'permisson_key' => 'c6-tindak-lanjut'
                    ],
                ]
            ],
            [
                'name_menu'     => 'C7',
                'link'          => '#',
                'is_parent'     => true,
                'sub_menu'      => [
                    [
                        'name_menu'     => 'Pernyataan Standard',
                        'link'          => 'c7/pernyataan-standard',
                        'is_parent'     => false,
                        'permisson_key' => 'c7-pernyataan-standard'
                    ],
                    [
                        'name_menu'     => 'Temuan',
                        'link'          => 'c7/temuan',
                        'is_parent'     => false,
                        'permisson_key' => 'c7-temuan'
                    ],
                    [
                        'name_menu'     => 'Tindak Lanjut',
                        'link'          => 'c7/tindak-lanjut',
                        'is_parent'     => false,
                        'permisson_key' => 'c7-tindak-lanjut'
                    ],
                ]
            ],
            [
                'name_menu'     => 'C8',
                'link'          => '#',
                'is_parent'     => true,
                'sub_menu'      => [
                    [
                        'name_menu'     => 'Pernyataan Standard',
                        'link'          => 'c8/pernyataan-standard',
                        'is_parent'     => false,
                        'permisson_key' => 'c8-pernyataan-standard'
                    ],
                    [
                        'name_menu'     => 'Temuan',
                        'link'          => 'c8/temuan',
                        'is_parent'     => false,
                        'permisson_key' => 'c8-temuan'
                    ],
                    [
                        'name_menu'     => 'Tindak Lanjut',
                        'link'          => 'c8/tindak-lanjut',
                        'is_parent'     => false,
                        'permisson_key' => 'c8-tindak-lanjut'
                    ],
                ]
            ],
            [
                'name_menu'     => 'C9',
                'link'          => '#',
                'is_parent'     => true,
                'sub_menu'      => [
                    [
                        'name_menu'     => 'Pernyataan Standard',
                        'link'          => 'c9/pernyataan-standard',
                        'is_parent'     => false,
                        'permisson_key' => 'c9-pernyataan-standard'
                    ],
                    [
                        'name_menu'     => 'Temuan',
                        'link'          => 'c9/temuan',
                        'is_parent'     => false,
                        'permisson_key' => 'c9-temuan'
                    ],
                    [
                        'name_menu'     => 'Tindak Lanjut',
                        'link'          => 'c9/tindak-lanjut',
                        'is_parent'     => false,
                        'permisson_key' => 'c9-tindak-lanjut'
                    ],
                ]
            ],
        ];

        foreach ($menu as $item) {
            $arr = Arr::only($item, ['name_menu', 'link', 'is_parent']);
            if (isset($item['permisson_key'])) {
                $arr['permisson_key'] = $item['permisson_key'];
                Permission::create(['name' => $item['permisson_key'] . '-view']);
            }
            $menu = Menu::create($arr);

            if (isset($item['sub_menu'])) {
                foreach ($item['sub_menu'] as $val) {
                    $arr = Arr::only($val, ['name_menu', 'link', 'is_parent', 'permisson_key']);
                    Permission::create(['name' => $arr['permisson_key'] . '-view']);
                    $arr['parent_id'] = $menu->id;
                    Menu::create($arr);
                }
            }
        }
    }
}
