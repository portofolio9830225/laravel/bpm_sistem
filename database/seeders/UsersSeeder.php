<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name'      => 'Superadmin',
            'username'  => 'superadmin',
            'password'  => Hash::make('password'),
            'email'     => 'superadmin@gmail.com',
            'role_id'   => 1,
        ]);

        $user->assignRole('Superadmin');

        $user = User::create([
            'name'      => 'Auditor',
            'username'  => 'auditor',
            'password'  => Hash::make('password'),
            'email'     => 'auditor@gmail.com',
            'role_id'   => 3,
        ]);

        $user->assignRole('Auditor');

        $user = User::create([
            'name'          => 'Prodi FTIK',
            'username'      => 'prodiftik',
            'password'      => Hash::make('password'),
            'email'         => 'prodiftik@gmail.com',
            'fakultas_id'   => 5,
            'prodi_id'      => 11,
            'role_id'       => 4,
        ]);

        $user->assignRole('Prodi');
    }
}
