<?php

namespace Database\Seeders;

use App\Models\Prodi;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class ProdiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'prodi_nama' => 'S1 Ilmu Hukum',
                'fakultas_id' => 1,
            ],
            [
                'prodi_nama' => 'S1 Manajemen',
                'fakultas_id' => 3,
            ],
            [
                'prodi_nama' => 'D3 Manajemen Perusahaan',
                'fakultas_id' => 3,
            ],
            [
                'prodi_nama' => 'S1 Akuntansi',
                'fakultas_id' => 3,
            ],
            [
                'prodi_nama' => 'S1 Teknik Sipil',
                'fakultas_id' => 2,
            ],
            [
                'prodi_nama' => 'S1 Teknik Elektro',
                'fakultas_id' => 2,
            ],
            [
                'prodi_nama' => 'S1 Perencanaan Wilayah dan Kota',
                'fakultas_id' => 2,
            ],
            [
                'prodi_nama' => 'S1 Teknologi Hasil Pertanian',
                'fakultas_id' => 4,
            ],
            [
                'prodi_nama' => 'S1 Psikologi',
                'fakultas_id' => 6,
            ],
            [
                'prodi_nama' => 'S1 Sistem Informasi',
                'fakultas_id' => 5,
            ],
            [
                'prodi_nama' => 'S1 Teknik Informatika',
                'fakultas_id' => 5,
            ],
            [
                'prodi_nama' => 'S1 Ilmu Komunikasi',
                'fakultas_id' => 5,
            ],
            [
                'prodi_nama' => 'S1 Pariwisata',
                'fakultas_id' => 5,
            ],
            [
                'prodi_nama' => 'S2 Hukum',
                'fakultas_id' => 7,
            ],
            [
                'prodi_nama' => 'S2 Manajemen',
                'fakultas_id' => 7,
            ],
            [
                'prodi_nama' => 'S2 Psikologi',
                'fakultas_id' => 7,
            ]
        ];

        foreach ($data as $item) {
            Prodi::create($item);
        }
    }
}
